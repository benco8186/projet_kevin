package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.IUserPersistence;
import com.example.demo.persistence.UserPersistence;
import com.example.demo.service.IUserService;
import com.example.demo.service.UserService;

@Configuration
public class DependencyInjectionConfig {
	@Bean
	public IUserService userService() {
		return new UserService();
	}
	
	@Bean
	public IUserPersistence userPersistence() {
		return new UserPersistence();
	}
}
