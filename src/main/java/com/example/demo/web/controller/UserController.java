package com.example.demo.web.controller;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.dto.UserDto;
import com.example.demo.model.UserBean;
import com.example.demo.model.UserViewModel;
import com.example.demo.service.IUserService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
@RestController
public class UserController {
	  @Autowired
	  IUserService userService;
		
	  
	  @GetMapping(value="/user/{id}")
	  public ResponseEntity<UserDto> GetUser(@PathVariable int id)
	  {
		  try {
			 UserDto userDto = userService.GetById(id);
			 if(userDto == null) {
				 return ResponseEntity.notFound().build();
			 }
			return ResponseEntity.ok(userDto);
		} catch (Exception e) {

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	  }
	  @PostMapping(value="/user")
	  public ResponseEntity<Void> CreateUser(@RequestBody UserViewModel userModel) {
		  try {
			  URI location = ServletUriComponentsBuilder
		                .fromCurrentRequest()
		                .path("/{id}")
		                .buildAndExpand(userService.Create(userModel))
		                .toUri();
			  return ResponseEntity.created(location).build();
		  }catch(Exception ex) {
			  return ResponseEntity.badRequest().build();
		  }
	  }
	  
	  @GetMapping(value="/users")
	  public ResponseEntity<List<UserDto>> GetUsers()
	  {
		  try {
			 List<UserDto> userDtos = userService.GetAll();
			return ResponseEntity.ok(userDtos);
		} catch (Exception e) {

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	  }
	  //public String afficherUnProduit(@PathVariable int id)
}
