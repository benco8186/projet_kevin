package com.example.demo.persistence;

import com.example.demo.model.UserBean;
import com.example.demo.model.UserViewModel;

public interface IUserPersistence extends IPersistenceBase<UserBean,UserViewModel> {

}
