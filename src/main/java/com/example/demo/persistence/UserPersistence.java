package com.example.demo.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.model.MessageBean;
import com.example.demo.model.RoleBean;
import com.example.demo.model.UserBean;
import com.example.demo.model.UserViewModel;
import com.example.demo.service.IUserService;

public class UserPersistence extends PersistenceBase implements IUserPersistence{

	@Override
	public int Create(UserViewModel vm) throws Exception {
		Connection con = null;
		int id = 0;
		  try{  
			  con = getConnection();			  
			  PreparedStatement stmt= con.prepareStatement("insert into user (nickname, password, role_id, register_date) values(?,?,?,?)");  
			  stmt.setString(1, vm.getName());
			  stmt.setString(2, vm.getPassword());
			  stmt.setInt(3, 2);
			  stmt.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			  stmt.execute();
			  Statement stmt2 = con.createStatement();
			  ResultSet result = stmt2.executeQuery("select id from user order by id desc limit 1");
			  if(result.first()) {
				  
				  id = result.getInt("id");
			  }
			  
			  }catch(Exception e){ 
				  throw new Exception(e.getMessage());
			  } finally {
				  closeConnection(con);
			  }

		  return id;
	  }

	@Override
	public UserBean GetById(int id) throws Exception {
		Connection con = null;
		UserBean user = null;
		  try{  
			  con = getConnection();			  			  
		
			  PreparedStatement stmt = con.prepareStatement(
					  "select u.id as userid, u.nickname, u.register_date, r.name as rolename, m.content, m.date from user as u "
					  + "left join role as r on r.id = u.role_id "
					  + "left join message as m on m.user_id = u.id "
					  + "where u.id = ? "
					  );
			  stmt.setInt(1, id);
			  ResultSet result = stmt.executeQuery();
			  List<UserBean> users = ConvertResultSetToEntity(result);
			  if(!users.isEmpty()) {
				  user = users.get(0);
			  }
			  
			  }catch(Exception e){ 
				  throw new Exception(e.getMessage());
			  } finally {
				  closeConnection(con);
			  }
		  return user;
	}

	@Override
	public List<UserBean> GetAll() throws Exception {
		Connection con = null;
		List<UserBean> users = null;
		  try{  
			  con = getConnection();			  			  
		
			  Statement stmt = con.createStatement();
			  ResultSet result = stmt.executeQuery(
					  "select u.id as userid, u.nickname, u.register_date, r.name as rolename, m.content, m.date from user as u "
					  + "left join role as r on r.id = u.role_id "
					  + "left join message as m on m.user_id = u.id "
					  );
			  users = ConvertResultSetToEntity(result);
			  }catch(Exception e){ 
				  throw new Exception(e.getMessage());
			  } finally {
				  closeConnection(con);
			  }
		  return users;
	}

	@Override
	public UserBean Delete(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserBean Update(UserViewModel vm) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private List<UserBean> ConvertResultSetToEntity(ResultSet result) throws SQLException{
		List<UserBean> users = new ArrayList<UserBean>();
		while(result.next()) {
			int id = result.getInt("userid");
			UserBean user = exist(users,id);
			  if(user == null) {
				  
				  user = new UserBean();
				  user.setId(id);
				  user.setNickname(result.getString("nickname"));
				  user.setRegisterDate(result.getTimestamp("register_date"));
				  RoleBean role = new RoleBean();
				  role.setName(result.getString("rolename"));
				  user.setRole(role);
				  user.setMessages(new ArrayList<MessageBean>());
				  users.add(user);
			  }
			  String messageContent = result.getString("content");
			  if(messageContent != null && !messageContent.isEmpty()) {
				  MessageBean message =  new MessageBean();
				  message.setContent(messageContent);
				  message.setDate(result.getTimestamp("date"));
				  user.getMessages().add(message);
			  }
		  }
		return users;
	}
	
	private UserBean exist(List<UserBean> users, int id) {
		for(UserBean user : users) {
			if(user.getId() == id) return user;
		}
		return null;
	}
	
}

