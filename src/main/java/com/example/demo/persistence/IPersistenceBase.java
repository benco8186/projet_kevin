package com.example.demo.persistence;

import java.util.List;

public interface IPersistenceBase<TEntity,TViewModel>  {
	TEntity GetById(int id) throws Exception;
	List<TEntity> GetAll() throws Exception;
	int Create(TViewModel vm) throws Exception ;
	TEntity Delete(int id);
	TEntity Update(TViewModel vm);
}
