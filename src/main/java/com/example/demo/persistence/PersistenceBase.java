package com.example.demo.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import com.example.demo.model.UserViewModel;

public abstract class PersistenceBase {
	
	protected Connection getConnection() throws ClassNotFoundException, SQLException {
		  Class.forName("com.mysql.cj.jdbc.Driver");  
		  Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/demo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","root");
		  return con;
	}

	  protected void closeConnection(Connection con) throws Exception {
		  if(con != null) {
			  try {
				con.close();
			} catch (SQLException e) {
				throw new Exception(e.getMessage());
				
			}
		  }
	  }
}
