package com.example.demo.model;

public class UserViewModel {
	private String name;
	private String password;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void set_name(String name) {
		this.name = name;
	}
	
}
