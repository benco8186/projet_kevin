package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.dto.MessageDto;
import com.example.demo.dto.UserDto;
import com.example.demo.model.MessageBean;
import com.example.demo.model.UserBean;
import com.example.demo.model.UserViewModel;
import com.example.demo.persistence.IUserPersistence;

public class UserService implements IUserService  {
	@Autowired
	IUserPersistence userPersistence;
	
	@Override
	public UserDto GetById(int id) throws Exception {
	
		UserBean userBean = userPersistence.GetById(id);
		UserDto userDto = convertToDto(userBean);
		//UserBean userBean = userPersistence.
		return userDto;
	}
	
	@Override
	public int Create(UserViewModel vm) throws Exception {
		return userPersistence.Create(vm);
	}

	@Override
	public List<UserDto> GetAll() throws Exception {
		List<UserBean> users = userPersistence.GetAll();
		List<UserDto> userDtos = new ArrayList<UserDto>();
		for(UserBean user : users) {
			userDtos.add(convertToDto(user));
		}
		return userDtos;
	}

	@Override
	public UserDto Delete(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDto Update(UserViewModel vm) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private UserDto convertToDto(UserBean userBean){
		UserDto userDto = null;
		if(userBean != null) {
			userDto = new UserDto();
			userDto.setNickname(userBean.getNickname());
			userDto.setRole(userBean.getRole().getName());
			userDto.setId(userBean.getId());
			userDto.setRegisterDate(userBean.getRegisterDate());
			userDto.setMessages(new ArrayList<MessageDto>());
			for(MessageBean message : userBean.getMessages()) {
				MessageDto messageDto = new MessageDto();
				messageDto.setContent(message.getContent());
				messageDto.setDate(message.getDate());
				userDto.getMessages().add(messageDto);
			}
		}
		return userDto;
	}
}
