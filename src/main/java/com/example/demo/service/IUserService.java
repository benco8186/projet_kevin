package com.example.demo.service;

import com.example.demo.dto.UserDto;
import com.example.demo.model.UserBean;
import com.example.demo.model.UserViewModel;

public interface IUserService extends IServiceBase<UserDto,UserViewModel> {
}
