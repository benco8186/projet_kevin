package com.example.demo.service;

import java.util.List;

import com.example.demo.model.UserBean;

public interface IServiceBase<TEntity, TViewModel> {
	TEntity GetById(int id) throws Exception;
	List<TEntity> GetAll() throws Exception;
	int Create(TViewModel vm) throws Exception;
	TEntity Delete(int id);
	TEntity Update(TViewModel vm);
}
