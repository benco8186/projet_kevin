create database demo;
use demo;

create table role(
	id int auto_increment primary key,
    name varchar(10) not null
    );

create table user(
	id int auto_increment primary key,
    nickname varchar(50) not null,
    password varchar(255) not null,
    register_date datetime not null,
    role_id int not null,
    foreign key role_id_fk (role_id)
		references  role (id)
	);
        
create table message(
	id int auto_increment primary key,
    content varchar(255) not null,
    date datetime not null,
    user_id int not null,
    foreign key user_id_fk (user_id)
		references  user (id)
    );

insert into role (name) values('admin');
insert into role (name) values('user');